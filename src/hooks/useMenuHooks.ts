import { reactive } from 'vue';
import menusConfigs from '@/assets/configs';

import saveIcon from "@/assets/menuIcon/save.png"
import exportIcon from "@/assets/menuIcon/export.png"
import previewIcon from "@/assets/menuIcon/preview.png"

export default function (){

    let menus = reactive(menusConfigs);

    return {
        menus,
        saveIcon,
        exportIcon,
        previewIcon
    }
}