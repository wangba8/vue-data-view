import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/manage',
      name: 'home',
      component: HomeView,
      children: [{
        path: '/manage',
        component: () => import('../views/home/manage.vue')
      },{
        path: '/template',
        component: () => import('../views/home/template.vue')
      },{
        path: '/apis',
        component: () => import('../views/home/apis.vue')
      }]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: () => import('../views/EditBigScreen.vue')
    },{
      path: '/404',
      name: '404',
      component: () => import('@/views/special/404.vue')
    },
    {
        path: '/:pathMatch(.*)',
        redirect: '/404'
    }
  ]
})

export default router
