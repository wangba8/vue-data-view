import title from "../../components/title";
import xAxis from "../../components/xAxis"
import yAxis from "../../components/yAxis"
import grid from "../../components/grid"
import legend from "../../components/legend"
import bar from "../components/bar"
import tooltip from "../../components/tooltip"

export default function (){
    
    return {
        title,
        xAxis,
        yAxis,
        grid,
        legend,
        bar,
        tooltip
    }
}