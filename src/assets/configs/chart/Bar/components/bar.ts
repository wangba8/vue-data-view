export default {
	"d3": false,
	"stack": false,
	"capsule": false,
	"type": "bar",
	"showBackground": false,
	"backgroundStyle": {
		"color": "rgba(0,0,0,1)"
	},
	"legendHoverLink": true,
	"itemStyle": {
		"color": "",
		"opacity": 1,
	},
	"barWidth": 30,
	"barGap": 0,
	"label": {
		"show": false,
		"fontSize": 12,
		"color": "rgba(255,255,255,1)",
		"fontWeight": "normal",
		"position": "top",
		"formatter1":`function(params){ 
	return params.value; 
}`
	},
};