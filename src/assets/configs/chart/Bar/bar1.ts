import useBarHooks from './barComponents/useBarHooks';

let {bar, xAxis, yAxis, tooltip, title, grid, legend} = useBarHooks();

tooltip.trigger = "axis";
export const bar1Config = {
	"option": {
		"hidden": false,
		"systemColor": "customed",
		// "systemColorFlag": false,
		"chart": {
			"textStyle": {
				"color": ""
			},
			"color": ['#e01f54', '#001852', '#f5e8c8', '#b8d2c7', '#c6b38e', '#a4d8c2', "#f3d999", '#d3758f', '#dcc392',
				'#2e4783', '#82b6e9', '#ff6347', '#a092f1', '#0a915d', '#eaf889', '#6699ff', '#ff6666', '#3cb371', '#d5b158',
				'#38b6b6'
			],
			"backgroundColor": "transparent",
			"series": [bar],
			"title": title,
			"xAxis": xAxis,
			"yAxis": yAxis,
			"grid": grid,
			"legend": legend,
			"tooltip": tooltip,
			"animation": true,
			"animationEasing": 'bounceOut',
		},
		"seriesList": [{
			"name": "系列1",
			"serieName": "系列一",
			"color": "rgba(26,117,252,1)",
			"position": 1,
			"color1": "rgba(255,99,71,1)",
			"position1": 1
		}]
	},
	"data": {
		"map": [{
				"field": "x",
				"value": "x",
				"desc": "类目",
				"state": "0"
			},
			{
				"field": "y",
				"value": "y",
				"desc": "值",
				"state": "0"
			},
			{
				"field": "s",
				"value": "s",
				"desc": "系列",
				"state": "0"
			}
		],
		"keyList": [],
		
		"dataType": "1",
		"static": {
"value": `[{
		"x": "上海",
		"y": 180,
		"s": "系列一"
	},
	{
		"x": "深圳",
		"y": 200,
		"s": "系列一"
	},
	{
		"x": "南京",
		"y": 160,
		"s": "系列一"
	},
	{
		"x": "成都",
		"y": 200,
		"s": "系列一"
	},
	{
		"x": "合肥",
		"y": 180,
		"s": "系列一"
	},
	{
		"x": "长沙",
		"y": 180,
		"s": "系列一"
	}
]`
		},

		"apiType": "0",
		"method": 'GET',
		"url": "",
		"openInterval": false,
		"time": 5000,
		//系统api的 配置项键值对
		"apiObject": {},
		"apiParams": `{}`,
		"apiParamsPre": `//参数对象名为param，可用参数见“全局参数查看”
`,
		"test": false,
		"dynamic": {
			"value": `[]`,
			"pretreatment": `//data为接口返回数据，处理data，最终data数据格式需为JSON数组
//如data=[{"name": "名称1", "value": 10}, {"name": "名称2", "value": 18}]`
		}
	},
	event: {
		animation: {
			type: "0",
			direction: "0",
			animationType: "",
			rateType: "",
			delay: 0,
			delayTime: '0s',
			insist: 1,
			insistTime: '1s',
			timing: "linear"
		},
		eventList: [],
	},
	component: {
		"name": "Bar1",
		"type": "Bar1",
		"x": 500,
		"y": 500,
		"width": 600,
		"height": 400,
		"icon": ''
	}
}