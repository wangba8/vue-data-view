export default {
	"iconSource": "0",
	"icon": "rect",
	"show": true,
	"positionType":"0",
	"align": "auto",
	"orient": "horizontal",
	"width": 300,
	"height": 50,
	"alignType":"2",
	"left": "right",
	"itemWidth":25,
	"itemHeight":14,
	"top": "top",
	"itemGap": 10,
	"type": "scroll",
	"pageTextStyle": {
		"color": "rgba(256,256,256,1)",
		"fontSize": 18
	},
	"textStyle": {
		"fontSize": 12,
		"color": "rgba(256,256,256,1)"
	}
};
