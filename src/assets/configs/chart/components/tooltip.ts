export default {
	"show": true,
	"trigger": "item",
	"textStyle": {
		"fontSize": 12,
		"color": "rgba(238, 224, 224, 1)"
	}
};