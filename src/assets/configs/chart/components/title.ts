export default {
	"positionType":"0",
	"top": "top",
	"left": "center",
	"alignType":"0",
	"show": false,
	"text": "标题",
	"textStyle": {
		"color": "#fff",
		"fontStyle": "normal",
		"fontWeight": "bold",
		"fontFamily": "标题",
		"fontSize": 18,
	},
	"subtext": "",
	"subtextStyle": {
		"color": "",
		"fontSize": 12
	}
};
