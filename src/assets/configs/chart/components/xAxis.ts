export default {
	//留白
	"boundaryGap": true,
	//轴名称
	"name": "",
	"nameShow": false,
	"nameTextStyle": {
		"fontSize": 14,
		"color": "rgba(256,256,256,1)",
		"paddingTop": 0,
		"paddingLeft": 0,
		"padding": [0, 0, 0, 0]
	},
	"position": "bottom",
	"type": 'category',
	"show": true,
	"inverse": false,
	"splitLine": {
		"show": false,
		"lineStyle": {
			"type": "dashed",
			"color": "rgba(255,255,255,1)",
			"width": 1
		}
	},
	"axisLabel": {
		//显示格式化
		"axisLabelType": "0",
		"axisLabelShowType": "YYYY-MM-DD",
		"show": true,
		"margin": 8,
		"rotate": 0,
		"fontSize": 14,
		"color": "rgba(255,255,255,1)",
		//间隔
		"interval": 0,
		//分行
		"wrap": false,
		"firstLine": 1
	},
	"axisLine": {
		"show": true,
		"lineStyle": {
			"color": "rgba(255,255,255,1)",
			"width": 1
		}
	},
	//坐标轴刻度
	"axisTick":{
		"show":true,
		"inside":true
	}
};