export default {
	"type": "value",
	//maxType 极值类型
	"maxType": "1",
	"minType": "0",
	"min": 0,
	"max": 0,
	//轴名称
	"name": "",
	"nameShow": false,
	"position": "left",
	"offset": 0,
	"interval1":0,
	"nameTextStyle": {
		"fontSize": 14,
		"color": "rgba(256,256,256,1)",
		"paddingTop": 0,
		"paddingLeft": 0,
		"padding": [0, 0, 0, 0]
	},
	"show": true,
	"inverse": false,
	"splitLine": {
		"show": true,
		"lineStyle": {
			"type": "dashed",
			"color": "rgba(256,256,256,.5)",
			"width": 1
		}
	},
	"axisLabel": {
		"show": true,
		"margin": 8,
		"fontSize": 14,
		"color": "rgba(256,256,256,1)",
		"fixedNum":0,
		"formatter":"{value}"
	},
	"axisLine": {
		"show": true,
		"lineStyle": {
			"color": "rgba(255,255,255,1)",
			"width": 1,
		}
	},
	//坐标轴刻度
	"axisTick":{
		"show":false,
		"inside":false
	}
};