import Bar from './Bar'
import Line from './Line'
import Pie from './Pie'
import Other from './OtherChart'

import chart from "@/assets/menuIcon/chart.png"

export default {
    name:"图表",
    icon:chart,
    children:[
        Bar,
        Line,
        Pie,
        Other
    ]
}