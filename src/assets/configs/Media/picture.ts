export default {
	"component": {
		"name": "VisualPicture",
		"x": 0,
		"y": 0,
		"width": 560,
		"height": 310
	},
	"option":{
		"hidden": false,
		"img": {
			"url": "",
			"radius": 0,
			"opacity":1,
			"scaleType":"stretch"
		},
	},
	"data": {
		"map": [{
			"field": "url",
			"value": "url",
			"desc": "地址",
			"state": "0"
		}],
		"keyList": [],
		
		"dataType": "1",
		"static": {
"value": `[
	{
		"url": ""
	}
]`
		},
		
		"apiType":"0",
		"method": 'GET',
		"url": "",
		"openInterval":false,
		"time": 5000, 	
		//系统api的 配置项键值对
		"apiObject":{},
		"apiParams":`{}`,
		"apiParamsPre": `//参数对象名为param，可用参数见“全局参数查看”
`,
		"test": false,
		"dynamic": {
			"value": `[]`,
			"pretreatment": `//data为接口返回数据，处理data，最终data数据格式需为JSON数组
//如data=[{"name": "名称1", "value": 10}, {"name": "名称2", "value": 18}]`
		}
	},
	
	event:{
		animation:{
			type:"0",
			direction:"0",
			animationType:"",
			rateType:"",
			delay:0,
			delayTime:'0s',
			insist:1,
			insistTime:'1s',
			timing:"linear"
		},
		//事件的列表 第一个是点击
		eventList:[{
			event:"click",
			title:'点击',
			indexList:[],
			eventType:"hiden",
			animationType:"",
			code:"",
			components:[{
				"name":"组件",
				"title":"组件",
				"eventType":"hiden",
				"enter":{
					"type":"0",
					//动画方向类型
					"direction":"0",
					//按照上面的配置最终得出的动画
					"animationType":"",
					//延迟的时间
					"delay":0,
					"delayTime":"0s",
					//持续的时间
					"insist":1,
					"insistTime":"1s",
					//速率种类
					"timing":"linear"
				},
				"out":{
					"type":"0",
					//动画方向类型
					"direction":"0",
					//按照上面的配置最终得出的动画
					"animationType":"",
					//延迟的时间
					"delay":0,
					"delayTime":"0s",
					//持续的时间
					"insist":1,
					"insistTime":"1s",
					//速率种类
					"timing":"linear"
				}
			}]
		}],
	}
}