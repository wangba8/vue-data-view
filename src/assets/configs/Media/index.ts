import media from '@/assets/menuIcon/media.png'

import picture from "./images/img.png"
export default {
    name:"媒体",
    icon:media,
    children:[
        {
            name:"图片",
            icon:picture,
            config: ()=>import('./picture'),
        }
    ]
}