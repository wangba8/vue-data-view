import map from '@/assets/menuIcon/map.png';
import basicMap from './images/map.png';
export default {
    name:"地图",
    icon:map,
    children:[
        {
            name:"基础地图",
            icon:basicMap,
            config: ()=>import('./map1'),
        }
    ]
}