export default {
	"option": {
		"hidden": false,
		"systemColor": "customed",
		// "systemColorFlag": false,
		"chart": {
			//背景颜色
			backgroundColor: 'transparent',
			tooltip: {
				triggerOn: "onmousemove",
			},
			direction:false,
			geo: {
				name: "地图",
				type: "map",
				map: "map",
				roam: false, //是否可缩放
				zoom: 1.11, //缩放比例
				itemStyle: {
					areaColor: 'transparent',
					borderColor: 'rgba(256,256,256,1)',
					borderWidth: 3,
					shadowColor: "rgba(185, 220, 227,1)"
				},
				label: {
					show: false, //显示省份标签
					textStyle: {
						color: "rgba(249, 249, 249,1)", //省份标签字体颜色
						fontSize: 12
					},
				},
				emphasis: {
					itemStyle: {

					}
				}
			},
			series: [{
					name: "地图",
					type: "map",
					map: "map1",
					roam: false, //是否可缩放
					zoom: 1.1, //缩放比例
					itemStyle: {
						areaColor: 'rgba(4,53,133,1)',
						borderColor: 'rgba(54,125,177,1)',
						borderWidth: 2
					},
					tooltip: {
						show: false
					},
					label: {
						show: false, //显示省份标签
						position: "right",
						color: "#fff",
						fontSize: 12,
						fontWeight: "normal"
					},
					emphasis: {
						label: {
							show: true, //显示省份标签
							color: "#fff",
							fontSize: 12,
							fontWeight: "normal"
						},
						itemStyle: {
							areaColor: 'rgba(4,53,133,1)',
							borderColor: 'rgba(54,125,177,1)',
							borderWidth: 2
						}
					}
				},
				{
					name: '',
					type: 'lines',
					zlevel: 1,
					effect: {
						show: true,
						period: 3, //箭头指向速度，值越小速度越快
						trailLength: 0.4, //特效尾迹长度[0,1]值越大，尾迹越长重
						symbol: 'arrow', //箭头图标
						symbolSize: 5, //图标大小
					},
					lineStyle: {
						color: 'rgba(221,233,18,1)',
						width: 2, //线条宽度
						opacity: 0.1, //尾迹线条透明度
						curveness: 0.3 //尾迹线条曲直度
					},
					tooltip: {
						show: false
					},
					data: []
				},
				{
					//自定义图片的字段
					imageName:"",
					type: "scatter",
					//坐标系类型
					coordinateSystem: 'geo',
					showEffectOn: 'render',
					zlevel: 2,
					rippleEffect: {
						period: 5,
						scale: 4,
						brushType: 'fill'
					},
					url: "",
					symbolType: "0",
					symbol: "circle",
					symbolSize: [15,15],
					hoverAnimation: true,
					itemStyle: {
						color: 'rgba(233, 83, 29, 1)',
						shadowBlur: 10,
						shadowColor: '#333'
					},
					label: {
						show: true,
						position: 'top',
						color: "#fff",
						fontSize: 12,
						fontWeight: "normal",
						formatter1:`function(params){
							return params.value;
						}`
					},
					data: [],
					"formatter": `function(params){
						return params.value
					}`
				},
				{
					//自定义图片的字段
					imageName:"",
					//类型
					type: 'effectScatter',
					//坐标系类型
					coordinateSystem: 'geo',
					showEffectOn: 'render',
					zlevel: 2,
					rippleEffect: {
						period: 5,
						scale: 4,
						brushType: 'fill'
					},
					url: "",
					symbolType: "0",
					symbol: "circle",
					symbolSize: [15,15],
					hoverAnimation: true,
					itemStyle: {
						color: '#1DE9B6',
						shadowBlur: 10,
						shadowColor: '#333'
					},
					label: {
						show: true,
						position: 'top',
						color: "#fff",
						fontSize: 12,
						fontWeight: "normal"
					},
					data: [],
					"formatter": `function(params){
						return params.value
					}`
				}
			]
		}
	},
	data: {
		"dataType": "1",
		"apiType": "0",
		"method": 'GET',
		"keyList": [],
		"url": "",
		"openInterval": false,
		"time": 5000,
		//系统api的 配置项键值对
		"apiObject": {},
		"apiParams": `{}`,
		"apiParamsPre": `//参数对象名为param，可用参数见“全局参数查看”
`,
		"test": false,
		"map": [{
				"field": "longitude",
				"value": "longitude",
				"desc": "经度",
				"state": "0"
			},
			{
				"field": "latitude",
				"value": "latitude",
				"desc": "纬度",
				"state": "0"
			},
			{
				"field": "info",
				"value": "info",
				"desc": "项目信息",
				"state": "0"
			},
			{
				"field": "type",
				"value": "type",
				"desc": "项目种类",
				"state": "0"
			}
		],
		"static": {
"value": `[
	{
		"longitude": "114.5435",
		"latitude": "22.5439",
		"info": {
			
		},
		"type":0
	},
	{
		"longitude": "114.3896",
		"latitude": "30.6628",
		"info": {
			
		},
		"type":1
	},
	{
		"longitude": "120.0586",
		"latitude": "32.5525",
		"info": {
			
		},
		"type":1
	},
	{
		"longitude": "119.4543",
		"latitude": "31.5582",
		"info": {
			
		},
		"type":1
	},
	{
		"longitude": "120.9155",
		"latitude": "30.6354",
		"info": {
			
		},
		"type":1
	}
]`
		},
		"dynamic": {
			"value": `[]`,
			"pretreatment": `//data为接口返回数据，处理data，最终data数据格式需为JSON数组
//如data=[{"name": "名称1", "value": 10}, {"name": "名称2", "value": 18}]`
		}
	},
	event: {
		animation: {
			type: "",
			rateType: "",
			duration: "",
			insist: ""
		},
		eventList: []
	},
	component: {
		"name": "Map2",
		"type": "Map2",
		"x": 500,
		"y": 500,
		"width": 600,
		"height": 400,
		"icon": '',
		//地图的id
		"mapName": "中国",
	}
}
