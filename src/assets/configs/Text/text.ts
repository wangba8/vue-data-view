export default {
	"option":{
		"hidden": false,
		"style": {
			"fontFamily":'Microsoft YaHei',
			"fontSize": 24,
			"fontStyle": false,
			"color": "#FFFFFF",
			"background": "rgba(255,255,255,0)",
			"letterSpacing": 0,
			"lineHeight": 32,
			"fontWeight": "normal",
			'alignType':'0',
			"textAlign": "left",
			"justifyContent": "center",
			"alignItems": "center",
			"shadow": {
				"open": false,
				"horizontal": 0,
				"vertical": 0,
				"distance": 0,
				"color": "#FFFFFF",
			},
			"gradual": {
				"open": false,
				"direction":"right",
				"color": "#00ffff",
				"animation": false, 
			},
		},
		"chart":{
			"animation":true,
			"animationEasing": 'bounceOut'
		}
	},
	"data": {
		"map": [{
				"field": "text",
				"value": "text",
				"desc": "值",
				"state": "0"
			}
		],
		"keyList": [],
		
		"dataType": "1",
		"static": {
"value": `[
	{
		"text": "我是文本"
	}
]`
		},
		
		"apiType":"0",
		"method": 'GET',
		"url": "",
		"openInterval":false,
		"time": 5000, 	
		//系统api的 配置项键值对
		"apiObject":{},
		"apiParams":`{}`,
		"apiParamsPre": `//参数对象名为param，可用参数见“全局参数查看”
`,
		"test": false,
		"dynamic": {
			"value": `[]`,
			"pretreatment": `//data为接口返回数据，处理data，最终data数据格式需为JSON数组
//如data=[{"name": "名称1", "value": 10}, {"name": "名称2", "value": 18}]`
		}	
	},
	event:{
		animation:{
			type:"0",
			direction:"0",
			animationType:"",
			rateType:"",
			delay:0,
			delayTime:'0s',
			insist:1,
			insistTime:'1s',
			timing:"linear"
		},
		//事件的列表
		eventList:[],
	},
	"component": {
		"name": "VisualText",
		"x": 0,
		"y": 0,
		"width": 200,
		"height": 48
	},
}