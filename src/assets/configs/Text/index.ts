import text from '@/assets/menuIcon/text.png';

import label from "./images/label.png";

export default {
    name:"文字",
    icon:text,
    children:[
        {
            name:"基础表格",
            icon:label,
            config: ()=>import('./text'),
        }
    ]
}