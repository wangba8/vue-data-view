import table from '@/assets/menuIcon/table.png';

import basicImage from './images/table.png';

export default {
    name:"表格",
    icon:table,
    children:[
        {
            name:"基础表格",
            icon:basicImage,
            config: ()=>import('./table'),
        }
    ]
}