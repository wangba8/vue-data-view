export default {
	"component": {
		"name": "CommonTable",
		"x": 0,
		"y": 0,
		"width": 600,
		"height": 300,
	},
	"option": {
		"formatterShow": false,
		"styleShow": false,
		"formatter": `//参数说明：prop:字符串，当前列对应的键名；cellValue:字符串或数字，当前单元格内容；
//index：数字，当前行号,比如将年龄那列加上‘岁’
if(prop == 'age'){
	cellValue += '岁'
}`,
		'stylefmt': `//参数说明：row：对象，当前行所有内容；colIndex:数字，当前列号；
//rowIndex：数字，当前行号；styleObj：样式对象，可添加或覆盖css属性
//比如将年龄大于30的当前行显示为空色
if(row.age > 30){
	styleObj.color = 'red'
}`,
		"hidden": false,//是否隐藏表格
		'tableConfig': {//表格配置
			'rightBorder': true,//显示边框
			"bottomBorder": true,
			"borderColor": "#fff",
			'indexShow': false,//显示序号
			"indexCfg": {
				'index': 1,//序号起始数字
				"color": "#fff",
				"fontSize": 14,
				"align": "center",
				"fontWeight": 'normal',
				"width": 50,
				"label": ""
			},
			'paginationShow': false,
			'pageSize': 20,
			"fyColor": '#fff',
			"fyFontSize": 14,
			"fyBgColor": "#1a75fc",
			'bgColor': 'rgba(0,0,0,0)'
		},
		'columnConfig': [{
			'prop': 'depart',
			'label': '部门',
			'align': 'center', //right,center
			'width': 200,
			"type": '1',
			"imageW": 20,
			"imageH": 20,
			"imageformat": `//参数说明：prexUrl:图片地址前缀；row:当前行对象；index:当前行号`,
			"fontFamily": "微软雅黑",
			"color": "#e6f8ff",
			"fontSize": 14,
			"url": "",//图片地址
			"fontWeight": "normal",
			guage: {
				center: [50, 50],
				radius: 10,
				startAngle: 225,
				endAngle: -45,
				min: 0,
				max: 100,
				"axisLine": {
					"lineStyle": {
						"color": [
							[0.25, '#1a75fc'],
							[0.5, '#01f1e3'],
							[0.75, '#ff2d2e'],
							[1, '#91c7ae']
						],
						"width": 5
					}
				},
				"detail": {
					"show": true,
					"offsetCenter": [0, 10],
					"color": "rgba(256,256,256,1)",
					fontSize: 16
				},
			}
		}, {
			'prop': 'name',
			'label': '姓名',
			'align': 'center', //right,center
			'width': 100,
			"type": '1',
			"imageW": 20,
			"imageH": 20,
			"imageformat": `//参数说明：prexUrl:图片地址前缀；row:当前行对象；index:当前行号`,
			"fontFamily": "微软雅黑",
			"color": "#e6f8ff",
			"fontSize": 14,
			"url": "",//图片地址
			"fontWeight": "normal",
			guage: {
				center: [50, 50],
				radius: 10,
				startAngle: 225,
				endAngle: -45,
				min: 0,
				max: 100,
				"axisLine": {
					"lineStyle": {
						"color": [
							[0.25, '#1a75fc'],
							[0.5, '#01f1e3'],
							[0.75, '#ff2d2e'],
							[1, '#91c7ae']
						],
						"width": 5
					}
				},
				"detail": {
					"show": true,
					"offsetCenter": [0, 10],
					"color": "rgba(256,256,256,1)",
					fontSize: 16
				},
			}
		}, {
			'prop': 'sex',
			'label': '性别',
			'align': 'center', //right,center
			'width': 100,
			"type": '1',
			"imageW": 20,
			"imageH": 20,
			"imageformat": `//参数说明：prexUrl:图片地址前缀；row:当前行对象；index:当前行号`,
			"fontFamily": "微软雅黑",
			"color": "#e6f8ff",
			"fontSize": 14,
			"url": "",//图片地址
			"fontWeight": "normal",
			guage: {
				center: [50, 50],
				radius: 10,
				startAngle: 225,
				endAngle: -45,
				min: 0,
				max: 100,
				"axisLine": {
					"lineStyle": {
						"color": [
							[0.25, '#1a75fc'],
							[0.5, '#01f1e3'],
							[0.75, '#ff2d2e'],
							[1, '#91c7ae']
						],
						"width": 5
					}
				},
				"detail": {
					"show": true,
					"offsetCenter": [0, 10],
					"color": "rgba(256,256,256,1)",
					fontSize: 16
				},
			}
		}, {
			'prop': 'age',
			'label': '年龄',
			'align': 'center', //right,center
			'width': 100,
			"type": '1',
			"imageW": 20,
			"imageH": 20,
			"imageformat": `//参数说明：prexUrl:图片地址前缀；row:当前行对象；index:当前行号`,
			"fontFamily": "微软雅黑",
			"color": "#e6f8ff",
			"fontSize": 14,
			"url": "",//图片地址
			"fontWeight": "normal",
			guage: {
				center: [50, 50],
				radius: 10,
				startAngle: 225,
				endAngle: -45,
				min: 0,
				max: 100,
				"axisLine": {
					"lineStyle": {
						"color": [
							[0.25, '#1a75fc'],
							[0.5, '#01f1e3'],
							[0.75, '#ff2d2e'],
							[1, '#91c7ae']
						],
						"width": 5
					}
				},
				"detail": {
					"show": true,
					"offsetCenter": [0, 10],
					"color": "rgba(256,256,256,1)",
					"fontSize": 16
				},
			}
		}],
		'headerConfig': {
			'height': 32,
			'bgColor': "#1a75fc",
			"fontFamily": "微软雅黑",
			// "color": "#909399",
			"color": '#e6f8ff',
			"fontSize": 14,
			"fontWeight": "bold",
			"textAlign": 'center',
		},
		'lineConfig': {
			'height': 32,
			'oddColor': 'transparent',
			'evenColor': 'transparent',
		},
		"chart": {
			"animation": true,
			"animationEasing": 'bounceOut'
		}
	},
	"data": {
		"dataType": "1",
		"apiType": "0",
		"method": 'GET',
		"url": "",
		"openInterval": false,
		"time": 5000,
		//系统api的 配置项键值对
		"apiObject": {},
		"apiParams": `{}`,
		"apiParamsPre": `//参数对象名为param，可用参数见“全局参数查看”
`,
		"test": false,
		"static": {
			"value": `[
	{
		"depart":"管理软件平台开发部",
		"name":"熊大",
		"sex": "男",
		"age": 30
	},{
		"depart":"管理软件平台开发部",
		"name":"熊二",
		"sex": "男",
		"age": 29
	},{
		"depart":"管理软件应用技术部",
		"name":"张三",
		"sex": "男",
		"age": 28
	},{
		"depart":"管理软件应用技术部",
		"name":"李四",
		"sex": "男",
		"age": 27
	},{
		"depart":"南京项目部",
		"name":"王五",
		"sex": "男",
		"age": 26
	},{
		"depart":"南京项目部",
		"name":"赵六",
		"sex": "男",
		"age": 25
	},{
		"depart":"北方项目部",
		"name":"肖七",
		"sex": "男",
		"age": 24
	},{
		"depart":"北方项目部",
		"name":"巴八",
		"sex": "男",
		"age": 31
	}
]`
		},
		"dynamic": {
			"value": `[]`,
			"pretreatment": `//data为接口返回数据，处理data，最终data数据格式需为JSON数组
//如data=[{"name": "名称1", "value": 10}, {"name": "名称2", "value": 18}]`
		}
	},
	event: {
		//动画
		animation: {
			//动画的整体类型
			type: "0",
			//动画方向类型
			direction: "0",
			//按照上面的配置最终得出的动画
			animationType: "",
			//延迟的时间
			delay: 0,
			delayTime: '0s',
			//持续的时间
			insist: 1,
			insistTime: '1s',
			//速率种类
			timing: "linear"
		},
		//事件的列表 第一个是点击
		//第二个是鼠标进入
		//第三个是鼠标离开
		eventList: []
	}
}
