export default {
	"component": {
		"name": "VisualButton",
		"x": 0,
		"y": 0,
		"width": 100,
		"height": 40
	},
	"option":{
		"hidden": false,
		"text": "按钮",
		"fontSize": 20,
		"borderShow":true,
		"color": "#fff",
		"borderColor":"#1a75fc",
		"backgroundColor": "rgba(0,0,0,0)",
		"borderRadius": 5,
		"borderWidth":2,
		"icon": {
			"open": false,
			"class": "el-icon-search",
			"color":"#fff"
		}
	},
	event:{
		//动画
		animation:{
			//动画的整体类型
			type:"0",
			//动画方向类型
			direction:"0",
			//按照上面的配置最终得出的动画
			animationType:"",
			//延迟的时间
			delay:0,
			delayTime:'0s',
			//持续的时间
			insist:1,
			insistTime:'1s',
			//速率种类
			timing:"linear"
		},
		//事件的列表 第一个是点击
		//第二个是鼠标进入
		//第三个是鼠标离开
		eventList:[{
			event:"click",
			title:'点击',
			indexList:[],
			eventType:"hiden",
			animationType:"",
			code:"",
			components:[{
				"name":"组件",
				"title":"组件",
				"eventType":"hiden",
				"enter":{
					"type":"0",
					//动画方向类型
					"direction":"0",
					//按照上面的配置最终得出的动画
					"animationType":"",
					//延迟的时间
					"delay":0,
					"delayTime":"0s",
					//持续的时间
					"insist":1,
					"insistTime":"1s",
					//速率种类
					"timing":"linear"
				},
				"out":{
					"type":"0",
					//动画方向类型
					"direction":"0",
					//按照上面的配置最终得出的动画
					"animationType":"",
					//延迟的时间
					"delay":0,
					"delayTime":"0s",
					//持续的时间
					"insist":1,
					"insistTime":"1s",
					//速率种类
					"timing":"linear"
				}
			}]
		}],
	}
}