import interact from "@/assets/menuIcon/interaction.png"

import button from "./images/button.png"

export default {
    name:"交互",
    icon:interact,
    children:[
        {
            name:"按钮",
            icon:button,
            config: ()=>import('./button'),
        }
    ]
}