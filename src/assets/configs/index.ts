import chart from "./chart"
import text from "./Text"
import media from "./Media"
import interact from "./Interact"
import table from "./Table"
import map from "./Map"


export default [
    chart,
    text,
    media,
    interact,
    table,
    map
]